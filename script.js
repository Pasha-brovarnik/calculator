const calculatorDisplay = document.querySelector('h1');
const inputBtns = document.querySelectorAll('button');
const clearBtn = document.querySelector('#clear-btn');

// Calculate first and scond calues depending on operator
const Calculate = {
	'/': (firstNumber, secondNumber) => firstNumber / secondNumber,
	'*': (firstNumber, secondNumber) => firstNumber * secondNumber,
	'+': (firstNumber, secondNumber) => firstNumber + secondNumber,
	'-': (firstNumber, secondNumber) => firstNumber - secondNumber,
	'=': (firstNumber, secondNumber) => secondNumber,
};

let firstValue = 0;
let operatorValue = '';
let awaitingNextValue = false;

function sendNumberValue(number) {
	// Replace current display value if first value is entered
	if (awaitingNextValue) {
		calculatorDisplay.textContent = number;
		awaitingNextValue = false;
	} else {
		// If display value is 0, replace it, otherwise add number
		const displayValue = calculatorDisplay.textContent;
		calculatorDisplay.textContent =
			displayValue === '0' ? number : displayValue + number;
	}
}

// Reset display
function resetAll() {
	calculatorDisplay.textContent = '0';
	firstValue = 0;
	operatorValue = '';
	awaitingNextValue = false;
}

// Add decimal
function addDecimal() {
	// if operator press, dont add decimal
	if (awaitingNextValue) {
		return;
	}

	if (!calculatorDisplay.textContent.includes('.')) {
		calculatorDisplay.textContent = `${calculatorDisplay.textContent}.`;
	}
}

function useOperator(operator) {
	// Prevent multiple operators
	if (operatorValue && awaitingNextValue) {
		operatorValue = operator;
		return;
	}

	const currentValue = Number(calculatorDisplay.textContent);
	// Assign first value if no value exists
	if (!firstValue) {
		firstValue = currentValue;
	} else {
		const calculation = Calculate[operatorValue](firstValue, currentValue);
		calculatorDisplay.textContent = calculation;
		firstValue = calculation;
	}
	// Ready for next value, store operator
	awaitingNextValue = true;
	operatorValue = operator;
}

// Event listeners for all buttons
inputBtns.forEach((btn) => {
	if (btn.classList.length === 0) {
		btn.addEventListener('click', () => sendNumberValue(btn.value));
	} else if (btn.classList.contains('operator')) {
		btn.addEventListener('click', () => useOperator(btn.value));
	} else if (btn.classList.contains('decimal')) {
		btn.addEventListener('click', addDecimal);
	}
});

clearBtn.addEventListener('click', resetAll);
